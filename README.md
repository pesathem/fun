# Seemingly useless `Bash` things

The snippets below may seem stupid...guess what, they are! 

## [The body of a function may be any compound command](https://www.gnu.org/software/bash/manual/html_node/Shell-Functions.html)

```bash
f() { echo hello;}; f
f() if :; then echo hello; fi; f
f() while echo hello; ! :; do :; done; f
```
**Output:**
```
hello
hello
hello
```

## [Stupid way to emulate an associative array](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion)

```bash
foo_bar=1a
foo_hello=2b
foo_bash=3c
 
for var in "${!foo_@}"; do
   echo "${var#*_} : ${!var}"
done
```
**Output:**
```
bar : 1a
bash : 3c
hello : 2b
```

## [Stupid way to display time](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion)

```bash
show_time() {
    local foo='\t'
    echo "${foo@P}"
}

show_time
```
**Output:**
```
20:48:38
```

## [Fun with aliases](https://www.gnu.org/software/bash/manual/html_node/Aliases.html)

```bash
alias f='g'
alias h='i '
alias i='echo'
alias j='foo'

f() {
    alias h='echo bar'
    h j
}

g
h
```
**Output:**
```
foo
bar
```

# Todo

## Bash Reference Manual
 - [ ] [1. Introduction](https://www.gnu.org/software/bash/manual/bashref.html#Introduction)
 - [ ] [2. Definitions](https://www.gnu.org/software/bash/manual/bashref.html#Definitions)
 - [ ] [3. Basic Shell Features](https://www.gnu.org/software/bash/manual/bashref.html#Basic-Shell-Features)
 - [ ] [4. Shell Builtin Commands](https://www.gnu.org/software/bash/manual/bashref.html#Shell-Builtin-Commands)
 - [ ] [5. Shell Variables](https://www.gnu.org/software/bash/manual/bashref.html#Shell-Variables)
 - [ ] [6. Bash Features](https://www.gnu.org/software/bash/manual/bashref.html#Bash-Features)
 - [x] [7. Job Control](https://www.gnu.org/software/bash/manual/bashref.html#Job-Control)
 - [ ] [8. Command Line Editing](https://www.gnu.org/software/bash/manual/bashref.html#Command-Line-Editing)
 - [ ] [9. Using History Interactively](https://www.gnu.org/software/bash/manual/bashref.html#Using-History-Interactively)
 - [ ] [10. Installing Bash](https://www.gnu.org/software/bash/manual/bashref.html#Installing-Bash)
 
## Bash
 - [ ] [Bash FAQ](https://mywiki.wooledge.org/BashFAQ)
 - [ ] [Bash Hackers](http://wiki.bash-hackers.org/)
 - [ ] Linux Command Line and Shell Scripting Bible
 - [ ] The Linux Command Line: A Complete Introduction

## Miscellaneous
 - [ ] Advanced Vim
 - [ ] GIT
 - [ ] [System Calls](https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/)
 - [ ] [Advanced Regex](https://www.regular-expressions.info/)
 - [ ] [Awk](http://www.grymoire.com/Unix/Awk.html)
 - [ ] Perl basics
 - [ ] Linux Administration Handbook
 - [ ] How Linux Works: What Every Superuser Should Know
 - [ ] [Linux From Scratch](http://www.linuxfromscratch.org/)
